// Import the standard library
local lib = import 'gmailctl.libsonnet';

local cbarrett = 'cbarrett@gitlab.com';
local craig = 'craig@gitlab.com';
local me = {
  or: [
    {to: cbarrett},
    {to: craig},
  ]
};

local markMessagesAsRead = true;

local instances =[
  'ops.gitlab.net',
  'dev.gitlab.org',
  'pre.gitlab.com',
  'staging.gitlab.com',
  'gitlab.com'
];

// utility that given a rule it returns its negated filter.
local negate(r) = { not: r };

// utility that given a list of rules returns their negations
local negatedList(rules) = [
  negate(r),
  for r in rules
];

local GitLab = {
  Inbox(prefix):: {
    filter: {
      to: '%s@gitlab.com' % [prefix],
    },
    actions: {
      archive: true,
      labels: [
        'Inboxes/%s' % [prefix]
      ]
    }
  },

  OpsContact(emailSuffix, labelSuffix='', markAsRead=false):: {
    filter: {
      to: 'ops-contact+%s@gitlab.com' % [emailSuffix],
    },
    actions: {
      labels: [
        if labelSuffix != '' then 'OpsContact/%s' % [labelSuffix] else 'OpsContact/%s' % [emailSuffix],
      ],
      archive: true,
      markRead: markAsRead
    },
  },

  OpsContactCatchall():: {
    filter: {
      to: 'ops-contact@gitlab.com',
    },
    actions: {
      labels: [
        'OpsContact',
      ],
      archive: true
    },
  },

  instanceFilter(instance):: {
    local queryRoot = 'You\'re receiving this email because',
    local queryMiddles = [
      'of your account',
      'you have been assigned an item',
      'you have been mentioned',
    ],

    local quotedOrStrings = std.join(' ', std.map(function(x) '"%s"' % x, queryMiddles)),

    query: '("%s" {%s} "on %s")' % [queryRoot, quotedOrStrings, instance],
  },

  Instance(instance, excludes=[]):: {
    local excludeFilters = [
      GitLab.instanceFilter(x)
      for x in excludes if x != instance
    ],
    filter: {
      and: [
        GitLab.instanceFilter(instance),
      ] + negatedList(excludeFilters),
    },
    actions: {
        archive: true,
        markSpam: false,
        markImportant: false,
        category: 'updates',
        labels: [
            'Instances/%s' % [instance],
        ],
    },
  },

  Project(path, markAsRead=false):: {
    filter: {
      query: '"You\'re receiving this email because of your account on" +list:\'%s/\' -list:ops-contact.gitlab.com' % [path],
    },
    actions: {
      archive: true,
      markImportant: false,
      markSpam: false,
      markRead: markAsRead,
      category: "updates",
     labels: [
        'Projects/%s' % [path],
      ],
    },
  },

  Conference(name, filter):: {
    filter: {
      query: '%s' % [filter],
    },
    actions: {
      star: true,
      markSpam: false,
      markImportant: true,
      category: "personal",
      labels: [
        'Conferences/%s' % [name],
      ],
    },
  },

  BlackholeFrom(fromList):: {
    filter: {
      query: 'from:{%s}' % std.join(' ', fromList)
    },
    actions: {
      markRead: true,
      markImportant: false,
      category: "promotions",
      labels: ['Blackhole'],
    },
  },
};

local rules = [
  // Any catchalls must come last!
  GitLab.OpsContact('aws', 'Amazon'),
  GitLab.OpsContact('cloudflare', 'Cloudflare'),
  GitLab.OpsContact('deadmanssnitch', 'DeadMansSnitch'),
  GitLab.OpsContact('gandi', 'Gandi'),
  GitLab.OpsContact('gcpnotification', 'GCP'),
  GitLab.OpsContact('letsencrypt', 'LetsEncrypt', markMessagesAsRead),
  GitLab.OpsContact('macstadium', 'MacStadium'),
  GitLab.OpsContact('mailgun', 'MailGun'),
  GitLab.OpsContact('packagecloudbackups', 'DeadMansSnitch'),
  GitLab.OpsContact('route53', 'Amazon'),

  GitLab.Project('gitlab-com/gitlab-com-infrastructure'),
  GitLab.Project('gitlab-com/gl-infra/ansible-workloads/db-provisioning'),
  GitLab.Project('gitlab-com/gl-infra/ansible-workloads/gitlab-config-management'),
  GitLab.Project('gitlab-com/gl-infra/delivery'),
  GitLab.Project('gitlab-com/gl-infra/deployer'),
  GitLab.Project('gitlab-com/gl-infra/feature-flag-log', markMessagesAsRead),
  GitLab.Project('gitlab-com/gl-infra/infrastructure'),
  GitLab.Project('gitlab-com/gl-infra/marquee-account-alerts'),
  GitLab.Project('gitlab-com/gl-infra/on-call-handovers'),
  GitLab.Project('gitlab-com/gl-infra/production'),
  GitLab.Project('gitlab-com/gl-infra/woodhouse'),
  GitLab.Project('gitlab-com/runbooks'),
  GitLab.Project('gitlab-com/www-gitlab-com'),
  GitLab.Project('gitlab-cookbooks/chef-repo'),
  GitLab.Project('gitlab-org/gitlab'),

  GitLab.Instance('ops.gitlab.net', instances),
  GitLab.Instance('dev.gitlab.org', instances),
  GitLab.Instance('gitlab.com', instances),
  GitLab.Instance('pre.gitlab.com', instances),
  GitLab.Instance('staging.gitlab.com', instances),

  GitLab.Conference('Contribute', "from: 'contribute@gitlab.com'"),
  GitLab.Conference('Monitorama', "to: 'craig+monitorama@gitlab.com'"),

  GitLab.Inbox('ops-notifications'),

  GitLab.BlackholeFrom([
    'bizkonnect.com',
    'dewintergroup.com',
    'gitiab.com',
    'kentik.io',
    'magnaquest.com',
    'migesa.com',
    'mktg-update@thoughtspot.com',
    'stoketalent.com',
  ]),

  {
    filter: {
      from: "o365mc@microsoft.com"
    },
    actions: {
      markSpam: false,
    
      labels: [
        "Vendors/Azure"
      ]
    }
  },
  {
    filter: {
      or: [
        {from: "no-reply@pagerduty.com"},
        {from: "notifications@pagerduty.com"},
      ],
    },
    actions: {
      markSpam: false,
      markImportant: true,
      category: "personal",
      labels: [
        "Vendors/PagerDuty"
      ]
    }
  },
  {
    filter: {
     from: "sentry@mg.gitlab.com",
    },
    actions: {
      archive: true,
      markSpam: false,
      markImportant: false,
      markRead: true,
      category: "updates",
      labels: [
        "Misc/Sentry"
      ]
    }
  },
  {
    filter: {
      and: [
        {to: "root@*"},
        {subject: "Cron"},
      ],
    },
    actions: {
      archive: true,
      markSpam: false,
      markImportant: false,
      markRead: true,
      category: "updates",
      labels: [
        "Misc/Crons"
      ]
    }
  },
 {
    filter: {
      or: [
        {has: "Invitation from Google Calendar"},
        {has: "iCal"},
      ],
    },
    actions: {
      star: true,
      markSpam: false,
      markImportant: true,
      category: "personal",
      labels: [
        "Meetings"
      ]
    }
  },
];

// Manual labels not referenced in rules.
local labels = [
  'expenses',
];

{
  version: 'v1alpha3',
  author: {
    name: 'Craig Barrett',
    email: craig,
  },
  labels: lib.rulesLabels(rules) + [{ name: l } for l in labels],
  rules: rules,
}